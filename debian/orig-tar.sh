#!/bin/sh -e

VERSION=$1
TAR=../rngom_$VERSION.orig.tar.xz
DIR=rngom-$VERSION
DATE=$(echo "$VERSION" | sed -e 's/^.*\+svn//;s/$/T2359Z/')
TAG=$(echo "rngom-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export -r {$DATE} https://svn.java.net/svn/rngom~svn/trunk/rngom $DIR
XZ_OPT=--best tar -c -J -f $TAR \
    --exclude '*.jar' \
    --exclude '*.class' \
    --exclude '*.ipr' \
    --exclude '*.iml' \
    --exclude '.settings' \
    --exclude '.project' \
    --exclude '.classpath' \
    $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
