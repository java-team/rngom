Source: rngom
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Timo Aaltonen <tjaalton@ubuntu.com>
Build-Depends: cdbs, debhelper (>= 9), default-jdk, javahelper, maven-debian-helper (>= 1.5)
Build-Depends-Indep: junit4,
                     libjavacc-maven-plugin-java,
                     libmaven-antrun-plugin-java,
                     libmsv-java,
                     librelaxng-datatype-java (>= 1.0),
                     libstax-java,
                     libxmlunit-java
Standards-Version: 3.9.5
Vcs-Git: git://anonscm.debian.org/pkg-java/rngom.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-java/rngom.git
Homepage: http://rngom.java.net

Package: librngom-java
Architecture: all
Depends: ${maven:Depends}, ${misc:Depends}
Recommends: ${maven:OptionalDepends}
Description: Java library for parsing RELAX NG grammars
 RNGOM is an open-source Java library for parsing RELAX NG grammars.
 .
 In particular, RNGOM can:
  * parse the XML syntax
  * parse the compact syntax
  * check all the semantic restrictions as specified in the specification
  * parse RELAX NG into application-defined data structures
  * build a default data structure based around the binarized simple syntax
    or another data structure that preserves more of the parsed information
  * parse foreign elements/attributes in a schema
  * parse comments in a schema
